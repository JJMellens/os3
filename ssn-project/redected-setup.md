## Getting re-DECTed up and running.

0. Pull the repo from https://github.com/znuh/re-DECTed

1. Install gnuradio with `apt install gnuradio`, this should fix the gnuradio requirement. Make sure to note what version you installed for step 2.

2. Install osmosdr from https://github.com/osmocom/gr-osmosdr. Make sure you get the right branch for your version (as of writing that would be gr3.8 for the gnuradio version from apt). To pull this branch use `git clone -b gr3.8 https://github.com/osmocom/gr-osmosdr.git`

3. Follow the installation instructions from the osmosdr wiki (copied below):
```
cd gr-osmosdr/
mkdir build
cd build/
cmake ../
make
sudo make install
sudo ldconfig
```
The `make` command might throw an error such as `no rule to make target '/usr/lib/x86_64-linux-gnu/liborc-0.4.so'`; in that case just install the specific packages with `apt` and try `make` again.

4. The Makefile will default to installing the osmosdr Python module for Python 3 (or at least it did for me), meaning that if you use a different Python version you will still get an ImportError if you tried to run the re-DECTed script now. You should be able to find the osmosdr module in `/usr/local/lib/python3/dist-packages/`, but check other Python versions if it's not there. Move it to the right version with `cp -r`; for me this was `cp -r /usr/local/lib/python3/dist-packages/osmosdr /usr/local/lib/python3.8/dist-packages/osmosdr`; change your version number as needed.

5. Now follow the re-DECTed instructions to start the script (copied below):

`make`\
create dummy0 interface: `sudo ip link add dummy0 type dummy`\
start the dummy0 interface: `sudo ifconfig dummy0 up`\
run dectrcv as root: `sudo ./dectrcv`\
start the SDR part:\
	If you use gnuradio version <= 3.7 : `./dectrx_37.py`\
	If you use gnuradio version >= 3.8 : `./dectrx_38.py`\
set channel, gain values and ppm\
enjoy the DECT packets in wireshark

## Version 3.9 for Ubuntu 22
We install version 3.9 from the ppa, then also install libsndfile1-dev to overcome an error in the CMakeFile.txt for osmosdr. Then we clone the latest version of osmosdr (it should be compatible with 3.9), build it, clone re-DECTed, build that, and run it.
```
sudo add-apt-repository ppa:gnuradio/gnuradio-releases-3.9
sudo apt update
sudo apt install gnuradio=3.9.8.0-0~gnuradio~jammy-1 gnuradio-dev=3.9.8.0-0~gnuradio~jammy-1 libsndfile1-dev -y
git clone https://github.com/osmocom/gr-osmosdr.git
cd gr-osmosdr/
mkdir build
cd build/
cmake ../
make
sudo make install
sudo ldconfig
cd ../..
git clone https://github.com/znuh/re-DECTed.git
cd re-DECTED/SDR
make
sudo ip link add dummy0 type dummy
sudo ifconfig dummy0 up
sudo ./dectrcv
./dectrx_38.py
```
